# Bullet hell #

This is an attempt at building a logically structured bullet hell type of game in Unity.

The game puts you in a cockpit of a customizable spaceship and pitches you against enemies with guns shooting uncomfortably large amounts of bullets. 

# Core ideas #
* Complex bullet patterns
* Customizable ship
    * Cockpit decides main weapon
    * Hull decides secondary weapon
    * Tail decides the emergency feature (bomb)

# Technical solutions #
* Optimized bullets
    * object pooling 
    * separating simple bullets from "smart" bullets to reduce required computations
* Use of ScriptableObjects to simplify creating bullets