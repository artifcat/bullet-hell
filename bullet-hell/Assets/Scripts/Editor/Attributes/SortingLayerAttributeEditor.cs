﻿using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;


[CustomPropertyDrawer(typeof(SortingLayerAttribute))]
public class SortingLayerAttributeEditor : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        var options = GetSortingLayerNames();
        var picks = new int[options.Length];
        var name = property.stringValue;
        var choice = -1;
        for (int i = 0; i < options.Length; i++)
        {
            picks[i] = i;
            if (name == options[i]) choice = i;
        }
        //If the name wasn't found it means the layer name got changed, default to first option on the list to prevent errors
        if(choice == -1) { choice = 0; }
        choice = EditorGUI.IntPopup(position, "Sorting Layer", choice, options, picks);
        property.stringValue = options[choice];
    }
    //Turns out list of sorting layers is an internal property in Unity that's not exposed in any way, need to use reflection to pull it out
    public string[] GetSortingLayerNames()
    {
        Type internalEditorUtilityType = typeof(InternalEditorUtility);
        PropertyInfo sortingLayersProperty = internalEditorUtilityType.GetProperty("sortingLayerNames", BindingFlags.Static | BindingFlags.NonPublic);
        return (string[])sortingLayersProperty.GetValue(null, new object[0]);
    }
}
