﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BulletPool", menuName = "Bullets/BulletPool")]
public class BulletPool : ScriptableObject
{
    public int BulletCount;   
    [Header("Pool properties")]
    [SerializeField]
    private int initialCapacity;
    [Tooltip("When set to 0 the pool will expand indefinitely.")]
    [SerializeField]
    private int maxCapacity;
    [Header("Bullet properties")]
    [SerializeField]
    private GameObject bulletPrefab;
    [SerializeField]
    [Layer]
    private int bulletLayer;
    [SerializeField]
    [SortingLayer]
    private string sortingLayer;
    private List<GameObject> pool;
    private int lastFreeIndex; // index of "best guess" element that should be free
    private int lastRequestFrameCount = -1;
    private bool createdNewObjectsThisFrame;
    private GameObject root;

    /// <summary>
    /// Creates the BulletPool object in current scene and populates it based on settings on this object.
    /// </summary>
    public void Initialize()
    {
        if(!root){
        root = new GameObject("BulletPool");
        pool = new List<GameObject>(initialCapacity);
        lastFreeIndex = 0;

        // Create the initial amount of bullet objects and set them inactive
        GameObject newBullet;
        for (int i = 0; i < initialCapacity; ++i)
        {
            newBullet = GameObject.Instantiate(bulletPrefab, root.transform);
            newBullet.SetActive(false);
            newBullet.layer = bulletLayer;
            newBullet.GetComponent<SpriteRenderer>().sortingLayerName = sortingLayer;
            pool.Add(newBullet);
        }
        }
    }

    public GameObject GetBullet(BulletProperties bulletProperties, Vector2 position, Quaternion rotation){
        GameObject bullet = GetInactive();
        SpriteRenderer sr = bullet.GetComponent<SpriteRenderer>();
        CircleCollider2D cl = bullet.GetComponent<CircleCollider2D>();
        Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
        sr.sprite = bulletProperties.Sprite;
        sr.color = bulletProperties.Tint;
        cl.radius = bulletProperties.ColliderRadius;
        cl.offset = bulletProperties.ColliderOffset;
        rb.velocity = Vector2.zero;
        bullet.transform.position = position;
        bullet.transform.rotation = rotation;
        bullet.SetActive(true);
        return bullet;
    }

    /// <summary>
    /// Locates and returns an inactive object from the pool, or instantiates a new one if all pooled objects are active.
    /// </summary>
    /// <exception cref="PoolMaximumReachedException">Thrown when all objects in pool are active and the pool's maximum capacity has been reached.</exception>
    private GameObject GetInactive(){
        BulletCount = pool.Count;
        UnityEngine.Profiling.Profiler.BeginSample("Bullet finding");
        if(lastRequestFrameCount != Time.frameCount)
        {
            createdNewObjectsThisFrame = false;
            lastRequestFrameCount = Time.frameCount;
        }
        // If a new object was already created on this frame, it means lookup can be skipped because it was already performed and found no inactive objects
        if(!createdNewObjectsThisFrame)
        {
            int newFreeIndex = -1; // Used to prevent changing of the lastFreeIndex before the second loop finishes
            // Search for unused element starting from the index that came next after the last free element
            for (int i = lastFreeIndex; i < pool.Count; ++i)
            {
                if (!pool[i].gameObject.activeSelf)
                {
                    newFreeIndex = i+1;
                    if(newFreeIndex==pool.Count)
                    {
                        lastFreeIndex = 0; // Reached end of the pool, search for next free element should start from the beginning
                    }
                    else
                    {
                        lastFreeIndex = newFreeIndex;
                    }
                    UnityEngine.Profiling.Profiler.EndSample();
                    return pool[i];
                }
            }
            // Didn't find a free element, look for it starting from the first index
            for (int i = 0; i < lastFreeIndex; ++i)
            {
                if (!pool[i].gameObject.activeSelf)
                {
                    lastFreeIndex = i+1; // This will never be higher than pool.Count because it'd already be processed by first loop
                    UnityEngine.Profiling.Profiler.EndSample();
                    return pool[i];
                }
            }
            // There seems to be no point in changing the lastFreeIndex here. Due to how the pool is iterated through, the objects ahead of lastFreeIndex are the "oldest" (most of the time) and therefore more likely to be available next time this function is called.
        }
        UnityEngine.Profiling.Profiler.EndSample();
        // Create a new element if pool capacity was not yet reached
        // maxCapacity equal 0 means there is no upper capacity defined and therefore new elements can be added indefinitely
        if(pool.Count < maxCapacity || maxCapacity == 0)
        {
            UnityEngine.Profiling.Profiler.BeginSample("Bullet creation");
            GameObject newBullet;
            newBullet = GameObject.Instantiate(bulletPrefab, root.transform);
            newBullet.SetActive(false);
            newBullet.layer = bulletLayer;
            newBullet.GetComponent<SpriteRenderer>().sortingLayerName = sortingLayer;
            pool.Add(newBullet);

            createdNewObjectsThisFrame = true;
            UnityEngine.Profiling.Profiler.EndSample();
            return newBullet;
        }
        // There are no free elements available in the pool and it reached maximum capacity
        throw new PoolMaximumReachedException();
    }

}

public class PoolMaximumReachedException : System.Exception
{
    public PoolMaximumReachedException(){}
}