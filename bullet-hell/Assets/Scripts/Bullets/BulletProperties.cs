﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BulletProperties", menuName = "Bullets/BulletProperties")]
public class BulletProperties : ScriptableObject
{
    public Sprite Sprite { get { return sprite; } }
    public Color Tint { get { return tint; } }
    public float ColliderRadius { get { return colliderRadius; } }
    public Vector2 ColliderOffset { get { return colliderOffset; } }

    [SerializeField]
    private Sprite sprite;
    [SerializeField]
    private Color tint;
    [SerializeField]
    private float colliderRadius;
    [SerializeField]
    private Vector2 colliderOffset;
}
