﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RBBulletTest : MonoBehaviour
{
    public BulletPool pool;
    public BulletProperties bulletType;
    //public GameObject RBBulletPrefab;
    [Range(0.01f,1.0f)]
    public float FireRate;
    public float Force;

    private float angleOffset;

    void Start()
    {
        pool.Initialize();
        InvokeRepeating("Fire", 0, FireRate);
        angleOffset = 0;
    }


    void Fire()
    {
        GameObject bullet;
        ++angleOffset;//Random.Range(0f, 9f);
        angleOffset %=9;
        for(int i = 0; i < 40; ++i){
            //GameObject newBullet = Instantiate(RBBulletPrefab, transform.position, Quaternion.identity);
            //Vector2 direction = Quaternion.Euler(0f, 0f, angleOffset+360f*(i/40f)) * transform.up;
            Quaternion rotation = Quaternion.Euler(0f, 0f, angleOffset+360f*(i/40f));
            Vector2 direction = rotation * transform.up;
            bullet = pool.GetBullet(bulletType, transform.position, rotation);
            bullet.GetComponent<Rigidbody2D>().AddForce(direction * Force, ForceMode2D.Impulse);
        }
    }
}
