﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerInput))]
public class PlayerMovement : MonoBehaviour
{
    [SerializeField]
    private PlayerMovementSettings movementSettings;
    [SerializeField]
    private GameObject mousePointerPrefab;
    private GameObject mousePointer;

    private PlayerInput input;
    

    void Start()
    {
        input = GetComponent<PlayerInput>();

        switch (movementSettings.ControlScheme)
        {
            case ControlScheme.Mouse:
                //TODO: Move to class responsible for game states
                Cursor.visible = false;
                mousePointer = Instantiate(mousePointerPrefab, input.MouseLocation, Quaternion.identity);
                break;
            case ControlScheme.Keyboard:
                //
                break;
        }
    }

    void Update()
    {
        switch (movementSettings.ControlScheme)
        {
            case ControlScheme.Mouse:
                mousePointer.transform.position = input.MouseLocation;
                transform.position = Vector3.MoveTowards(transform.position, mousePointer.transform.position, Time.deltaTime * movementSettings.MouseMovementSpeed);
                break;
            case ControlScheme.Keyboard:
                transform.position = transform.position + ((Vector3)input.KeyboardInput * Time.deltaTime * movementSettings.KeyboardMovementSpeed);
                break;
        }
    }
}
