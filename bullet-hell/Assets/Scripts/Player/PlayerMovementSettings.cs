﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PlayerMovementSettings", menuName ="PlayerMovementSettings")]
public class PlayerMovementSettings : ScriptableObject
{
    public ControlScheme ControlScheme = ControlScheme.Mouse;
    [Range(0.1f,100)]
    public float MouseMovementSpeed = 1.0f;
    [Range(0.1f, 100)]
    public float KeyboardMovementSpeed = 1.0f;
}

public enum ControlScheme { Mouse, Keyboard };