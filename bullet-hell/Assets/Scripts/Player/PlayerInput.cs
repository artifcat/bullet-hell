﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    public Vector2 KeyboardInput { get; private set; }
    public Vector2 MouseLocation { get; private set; }
    private Vector2 _keyboardInput;
    private Camera mainCamera;

    void Start()
    {
        mainCamera = Camera.main;
    }

    void Update()
    {
        _keyboardInput.x = Input.GetAxis("Horizontal");
        _keyboardInput.y = Input.GetAxis("Vertical");
        KeyboardInput = _keyboardInput;

        MouseLocation = mainCamera.ScreenToWorldPoint(Input.mousePosition);
    }
}
